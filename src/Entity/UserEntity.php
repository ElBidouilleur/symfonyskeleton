<?php

namespace App\Entity;

use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserEntity implements UserInterface, PasswordAuthenticatedUserInterface
{
    private int $id;

    private string $role;

    private string $email;

    private string $hashedPassword;

    private string $firstName;

    private string $lastName;

    private string $connectionToken;

    public function __construct(
        int $id,
        string $connectionToken,
        string $role,
        string $hashedPassword,
        string $email,
        string $firstName,
        string $lastName
    ) {
        $this->id              = $id;
        $this->connectionToken = $connectionToken;
        $this->hashedPassword  = $hashedPassword;
        $this->role            = $role;
        $this->email           = $email;
        $this->firstName       = $firstName;
        $this->lastName        = $lastName;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getHashedPassword(): string
    {
        return $this->hashedPassword;
    }

    /**
     * @return string
     */
    public function getRole(): string
    {
        return $this->role;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getConnectionToken(): string
    {
        return $this->connectionToken;
    }

    /**
     * @param string $connectionToken
     */
    public function setConnectionToken(string $connectionToken): void
    {
        $this->connectionToken = $connectionToken;
    }

    public function getPassword(): ?string
    {
        return $this->hashedPassword;
    }

    /**
     * @param string $hashedPassword
     */
    public function setHashedPassword(string $hashedPassword): void
    {
        $this->hashedPassword = $hashedPassword;
    }

    public function getRoles(): array
    {
        return array_unique([
            $this->role
        ]);
    }

    /**
     * @param string $role
     */
    public function setRole(string $role): void
    {
        $this->role = $role;
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function getUserIdentifier(): string
    {
        return $this->email;
    }

    public function toJson(): array
    {
        return [
            'id'               => $this->id,
            'first_name'       => $this->firstName,
            'last_name'        => $this->lastName,
            'connection_token' => $this->connectionToken
        ];
    }
}