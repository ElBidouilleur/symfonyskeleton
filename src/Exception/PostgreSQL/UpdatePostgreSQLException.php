<?php

namespace App\Exception\PostgreSQL;

use Exception;
use Throwable;

class UpdatePostgreSQLException extends Exception implements Throwable
{
    public function __construct(string $message = "", int $code = 0, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}