<?php

namespace App\Service;

use App\Entity\UserEntity;
use App\Repository\AbstractRepository;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTEncodeFailureException;
use Symfony\Component\Security\Core\Security;

abstract class AbstractService
{
    protected AbstractRepository $repository;

    protected Security $security;

    protected JWTEncoderInterface $JWTEncoder;

    protected function getUserConnected(): ?UserEntity
    {
        $user = $this->security->getUser();

        if ($user instanceof UserEntity) {
            return $user;
        }

        return null;
    }

    /**
     * @param string $email
     * @param string $firstName
     * @param string $lastName
     *
     * @return string
     * @throws JWTEncodeFailureException
     */
    protected function jwtEncoding(string $email, string $firstName, string $lastName): string
    {
        return $this->JWTEncoder->encode([
            'email'     => $email,
            'firstName' => $firstName,
            'lastName'  => $lastName,
            'username'  => $email
        ]);
    }

    public function getRepository(): AbstractRepository
    {
        return $this->repository;
    }

}