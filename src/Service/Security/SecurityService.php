<?php

namespace App\Service\Security;

use App\Repository\Security\SecurityRepository;
use App\Service\AbstractService;
use Luna\Component\Manager\TypeManager;

class SecurityService extends AbstractService
{
    public function __construct(SecurityRepository $securityRepository)
    {
        $this->repository = $securityRepository;
    }

    public function userExist(string $email, string $firstName, string $lastName): bool
    {
        $query = $this->repository->userExist($email, $firstName, $lastName);

        return !TypeManager::isEmpty($query);
    }
}