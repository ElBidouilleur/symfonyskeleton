<?php

namespace App\Checker;

use Attribute;

#[Attribute]
class CheckerElement
{
    private string $jsonName;
    private ?string $regexp = null;
    private ?string $default = null;
    private ?string $samer = null;
    private ?string $contains = null;
    private ?string $notContains = null;

    private bool $required = true;
    private bool $nullable = false;
    private bool $canEmpty = false;
    private bool $optional = false;

    private ?int $minLength = null;
    private ?int $maxLength = null;

    public function __construct(
        string $jsonName,
        ?string $regexp = null,
        ?string $default = null,
        ?string $samer = null,
        ?string $contains = null,
        ?string $notContains = null,

        int $minLength = null,
        int $maxLength = null,

        bool $required = true,
        bool $nullable = false,
        bool $canEmpty = false,
        bool $optional = false
    ) {
        $this->jsonName    = $jsonName;
        $this->regexp      = $regexp;
        $this->default     = $default;
        $this->samer       = $samer;
        $this->contains    = $contains;
        $this->notContains = $notContains;

        $this->minLength = $minLength;
        $this->maxLength = $maxLength;

        $this->required = $required;
        $this->nullable = $nullable;
        $this->canEmpty = $canEmpty;
        $this->optional = $optional;
    }
}