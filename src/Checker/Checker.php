<?php

namespace App\Checker;

use App\Exception\CheckerException;
use Luna\Component\Manager\ValueManager;
use ReflectionClass;

class Checker
{
    /**
     * @throws CheckerException
     */
    public static function check(object $checkerElement, array $json): void
    {
        $reflection = new ReflectionClass($checkerElement);

        foreach ($reflection->getProperties() as $property) {
            $attributes = $property->getAttributes();
            $arguments  = $attributes[0]->getArguments();

            if ($arguments["required"] ?? true) {
                $currentBodyValue = $json[$arguments["jsonName"]];
            } else {
                $currentBodyValue = $json[$arguments["jsonName"]] ?? $arguments["default"];
            }

            foreach ($arguments as $name => $value) {
                if ($value !== null) {
                    switch ($name) {
                        case "regexp":
                            if (!ValueManager::match($value, $currentBodyValue)) {
                                throw new CheckerException("$currentBodyValue not match with regexp ($value)");
                            }
                            break;
                        case "minLength":
                            if (strlen(ValueManager::getString($currentBodyValue)) < $value) {
                                throw new CheckerException("$currentBodyValue not respect the minimum length $value");
                            }
                            break;
                        case "maxLength":
                            if (strlen(ValueManager::getString($currentBodyValue)) > $value) {
                                throw new CheckerException("$currentBodyValue not respect the maximum length $value");
                            }
                            break;
                        case "samer":
                            if ($currentBodyValue !== $json[$value]) {
                                throw new CheckerException("$currentBodyValue don't is same than $json[$value]");
                            }
                            break;
                        case "contains":
                            if (!str_contains($currentBodyValue, $json[$value])) {
                                throw new CheckerException("$currentBodyValue not contains {$json[$value]}");
                            }
                            break;
                        case "notContains":
                            if (str_contains($currentBodyValue, $json[$value])) {
                                throw new CheckerException("$currentBodyValue contains {$json[$value]}");
                            }
                            break;
                        default:
                            break;
                    }
                }
            }

            $property->setValue($checkerElement, $currentBodyValue);
        }
    }
}