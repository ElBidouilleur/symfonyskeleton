<?php

namespace App\Security;

use App\Entity\UserEntity;
use App\Service\PublicAccess\Administrator\PublicAccessAdministratorService;
use Exception;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UserProvider implements UserProviderInterface, PasswordUpgraderInterface
{

    private PublicAccessAdministratorService $userAccountService;

    /**
     * @param PublicAccessAdministratorService $userAccountService
     */
    public function __construct(PublicAccessAdministratorService $userAccountService)
    {
        $this->userAccountService = $userAccountService;
    }


    /**
     * @param string $identifier
     *
     * @return UserInterface
     */
    public function loadUserByIdentifier(string $identifier): UserInterface
    {
        // Load a User object from your data source or throw UserNotFoundException.
        // The $identifier argument is whatever value is being returned by the
        // getUserIdentifier() method in your User class.
        $user = $this->userAccountService->detailByEmail($identifier);

        return $user;
    }


    /**
     * @param UserInterface $user
     *
     * @return UserInterface
     * @throws Exception
     */
    public function refreshUser(UserInterface $user): UserInterface
    {
        if (!$user instanceof UserEntity) {
            throw new UnsupportedUserException(sprintf('Invalid user class "%s".', get_class($user)));
        }

        // Return a User object after making sure its data is "fresh".
        // Or throw a UserNotFoundException if the user no longer exists.
        throw new Exception('TODO: fill in refreshUser() inside ' . __FILE__);
    }


    /**
     * @param string $class
     *
     * @return bool
     */
    public function supportsClass(string $class): bool
    {
        return UserEntity::class === $class || is_subclass_of($class, UserEntity::class);
    }

    /**
     * @param PasswordAuthenticatedUserInterface $user
     * @param string                             $newHashedPassword
     *
     * @return void
     */
    public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newHashedPassword): void
    {
        // TODO: when hashed passwords are in use, this method should:
        // 1. persist the new password in the user storage
        // 2. update the $user object with $user->setPassword($newHashedPassword);
    }
}