<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController as SymfonyController;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\FileBag;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractController extends SymfonyController
{
    protected Request $request;

    private const MESSAGE = [
        Response::HTTP_OK             => "Resource found",
        Response::HTTP_ACCEPTED       => "Resource accepted",
        Response::HTTP_CREATED        => "Resource create",
        Response::HTTP_NO_CONTENT     => "No content",
        Response::HTTP_NOT_FOUND      => "Resource not found",
        Response::HTTP_NOT_ACCEPTABLE => "Request not acceptable",
        Response::HTTP_BAD_REQUEST    => "The client malformed request"
    ];

    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
    }

    public function getJsonBody(): array
    {
        return json_decode($this->request->getContent(), true);
    }

    public function getFiles(): FileBag
    {
        return $this->request->files;
    }

    public function getFile(string $key): File
    {
        return $this->request->files->get($key);
    }

    public function returnJson(int $status, ?array $message = null): JsonResponse
    {
        if ($message === null) {
            $jsonStringify = json_encode([
                'message' => self::MESSAGE[$status]
            ]);
            return JsonResponse::fromJsonString($jsonStringify, $status);
        }

        $jsonStringify = json_encode($message);
        return JsonResponse::fromJsonString($jsonStringify, $status);
    }

    public function get(string $key)
    {
        return $this->request->get($key);
    }
}