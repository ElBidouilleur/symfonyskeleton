<?php

namespace App\Repository\Security;

class SecuritySQL
{
    public static function SQL_userExist(): string
    {
        $table = $_ENV['USER_TABLE'];

        return "
            SELECT
                42
            FROM $table
            WHERE email = :email
                AND first_name = :first_name
                AND last_name = :last_name
        ";
    }
}