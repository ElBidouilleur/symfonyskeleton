<?php

namespace App\Repository\Security;

use App\Repository\AbstractRepository;

class SecurityRepository extends AbstractRepository
{
    public function userExist(string $email, string $firstName, string $lastName)
    {
        $variables = [
            'email'      => $email,
            'first_name' => $firstName,
            'last_name'  => $lastName
        ];
        $options   = [
            self::FETCH_ONE => true
        ];

        return $this->execute(SecuritySQL::SQL_userExist(), $variables, $options);
    }
}