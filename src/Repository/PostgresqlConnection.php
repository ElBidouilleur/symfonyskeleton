<?php

namespace App\Repository;

use Luna\Component\Manager\ValueManager;
use PDO;

class PostgresqlConnection
{
    private static ?PDO $connection = null;

    private static function initialize(): void
    {
        if (self::$connection === null) {
            $host         = ValueManager::getString($_ENV['DATABASE_HOST']);
            $port         = ValueManager::getString($_ENV['DATABASE_PORT']);
            $databaseName = ValueManager::getString($_ENV['DATABASE_NAME']);
            $username     = ValueManager::getString($_ENV['DATABASE_USERNAME']);
            $password     = ValueManager::getString($_ENV['DATABASE_PASSWORD']);

            self::$connection = new PDO("pgsql:host=$host;port=$port;dbname=$databaseName;user=$username;password=$password");
        }
    }

    public static function getConnection(): PDO
    {
        if (self::$connection === null) {
            self::initialize();
        }
        return self::$connection;
    }
}