<?php

namespace App\Repository;

use Exception;
use Luna\Component\Manager\TypeManager;
use Luna\Component\Manager\ValueManager;
use PDO;
use PDOStatement;

abstract class AbstractRepository
{
    protected const FETCH     = 'fetch';
    protected const FETCH_ONE = 'fetch_one';

    protected const VARIABLE_TYPE = 'type';

    protected const VARIABLE_VALUE = 'value';

    private PDO $pdo;

    public function __construct()
    {
        $this->pdo = PostgresqlConnection::getConnection();
    }

    public function findField(string $table, string $field, int $id): array
    {
        $variables = [
            'id' => [
                self::VARIABLE_TYPE  => PDO::PARAM_INT,
                self::VARIABLE_VALUE => $id
            ]
        ];
        $options   = [
            self::FETCH_ONE => true
        ];

        return ValueManager::getArray($this->execute(AbstractSQL::SQL_findField($table, $field), $variables, $options));
    }

    public function findUserConnected(string $email): array
    {
        $variables = [
            'email' => $email
        ];
        $options   = [
            self::FETCH_ONE => true
        ];

        return $this->execute(AbstractSQL::SQL_findUserConnected(), $variables, $options);
    }

    protected function newId(string $tableName): int
    {
        $options = [
            self::FETCH_ONE => true
        ];

        $newId = $this->execute(AbstractSQL::SQL_newId("sequence_{$tableName}_id"), [], $options);
        return ValueManager::getInteger($newId['nextval']);
    }

    protected function execute(
        string $sqlCommand,
        array $variables = [],
        array $options = [],
        bool &$status = null
    ): array|bool {
        $prepare = $this->pdo->prepare($sqlCommand);

        $this->buildParams($prepare, $variables);

        $status = $prepare->execute();

        if (in_array(self::FETCH_ONE, $options) && $options[self::FETCH_ONE]) {
            return ValueManager::getArray($prepare->fetch(PDO::FETCH_ASSOC));
        }

        if (!in_array(self::FETCH, $options) || $options[self::FETCH]) {
            return ValueManager::getArray($prepare->fetchAll(PDO::FETCH_ASSOC));
        }

        return [];
    }


    /**
     * @param string               $table
     * @param string               $field
     * @param string|bool|int|null $value
     * @param int                  $id
     *
     * @return void
     * @throws Exception
     */
    public function updateField(string $table, string $field, string|bool|int|null $value, int $id): void
    {
        $variables['value'][self::VARIABLE_TYPE] = match (gettype($value)) {
            'boolean' => PDO::PARAM_BOOL,
            'integer' => PDO::PARAM_INT,
            'string' => PDO::PARAM_STR,
            default => PDO::PARAM_NULL
        };

        $variables['value'][self::VARIABLE_VALUE] = $value;
        $variables['id']                          = $id;

        $options = [
            self::FETCH => false
        ];

        $this->execute(AbstractSQL::SQL_updateField($table, $field), $variables, $options, $status);

        if (!$status) {
            throw new Exception('Edit field don\'t work');
        }
    }

    public function deleteLine(int $id, string $table): void
    {
        $variables = [
            'id' => [
                self::VARIABLE_TYPE  => PDO::PARAM_INT,
                self::VARIABLE_VALUE => $id
            ]
        ];
        $options   = [
            self::FETCH => false
        ];

        $this->execute(AbstractSQL::SQL_deleteLine($table), $variables, $options);
    }


    /**
     * @param string $table
     * @param string $field
     * @param bool   $value
     * @param int    $id
     *
     * @return void
     * @throws Exception
     */
    public function updateFieldBoolean(string $table, string $field, bool $value, int $id): void
    {
        $variables = [
            'value' => [
                self::VARIABLE_TYPE  => PDO::PARAM_BOOL,
                self::VARIABLE_VALUE => $value
            ],
            'id'    => $id
        ];
        $options   = [
            self::FETCH => false
        ];

        $this->execute(AbstractSQL::SQL_updateField($table, $field), $variables, $options, $status);

        if (!$status) {
            throw new Exception('Edit field don\'t work');
        }
    }

    private function buildParams(PDOStatement &$prepare, array $variables): void
    {
        foreach ($variables as $key => &$value) {
            if (TypeManager::isArray($value)) {
                $prepare->bindValue($key, $value[self::VARIABLE_VALUE], $value[self::VARIABLE_TYPE]);
                continue;
            }

            $prepare->bindValue($key, $value);
        }
    }
}