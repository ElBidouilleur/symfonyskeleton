<?php

namespace App\Repository;

abstract class AbstractSQL
{
    public static function SQL_newId(string $table): string
    {
        return "
            SELECT NEXTVAL('$table')
        ";
    }

    public static function SQL_findField(string $table, string $field): string
    {
        return "
            SELECT
                $field
            FROM $table
            WHERE id = :id
                AND $field IS NOT NULL
        ";
    }

    public static function SQL_findUserConnected(): string
    {
        return "
            SELECT
                id,
                role,
                email,
                password,
                first_name,
                last_name,
                connection_token
            FROM public.administrator
            WHERE email = :email
        ";
    }

    public static function SQL_updateField(string $tableName, string $field): string
    {
        return "
            UPDATE public.$tableName SET $field = :value WHERE id = :id
        ";
    }

    public static function SQL_deleteLine(string $tableName): string
    {
        return "
            DELETE FROM public.$tableName WHERE id = :id
        ";
    }
}