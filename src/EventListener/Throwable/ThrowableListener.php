<?php

namespace App\EventListener\Throwable;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

class ThrowableListener
{
    public function onKernelException(ExceptionEvent $event) {
        $response = null;

        if (str_contains($event->getRequest()->getRequestUri(), "/api")) {
            $response = new JsonResponse([
                'code' => $event->getThrowable()->getCode(),
                'message' => $event->getThrowable()->getMessage(),
                'class' => $event->getThrowable()->getFile(),
                'line' => $event->getThrowable()->getLine()
            ]);
        }

        if (is_null($response)) {
            return;
        }

        $event->setResponse($response);
    }

}