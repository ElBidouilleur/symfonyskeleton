<?php

namespace App\Builder;

use ReflectionClass;

class Builder
{
    public static function toJson(object $builder): array
    {
        $list = [];

        $reflection = new ReflectionClass($builder);

        foreach ($reflection->getProperties() as $property) {
            $list[strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $property->getName()))] = $property->getValue($builder);
        }

        return $list;
    }
}